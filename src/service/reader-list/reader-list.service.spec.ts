import { Test, TestingModule } from '@nestjs/testing';
import { ReaderListService } from './reader-list.service';

describe('ReaderListService', () => {
  let service: ReaderListService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ReaderListService],
    }).compile();

    service = module.get<ReaderListService>(ReaderListService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
