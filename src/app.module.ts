import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ManagementController } from './management/management.controller';
import { ManagementModule } from './management/management.module';
import { ReaderListService } from './service/reader-list/reader-list.service';
import { UserListModule } from './module/user-list/user-list.module';
import { MessageBoardModule } from './module/message-board/message-board.module';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { ThrottlerGuard, ThrottlerModule } from '@nestjs/throttler';
import { APP_GUARD } from '@nestjs/core';

@Module({
  imports: [
    ManagementModule,
    MongooseModule.forRoot(
      'mongodb://127.0.0.1:27017/nest'
      // {

      // }
    ),
    UserListModule,
    AuthModule,
    UsersModule,
    ThrottlerModule.forRoot({
      ttl: 30,
      limit: 5
    })
    // MessageBoardModule
  ],
  controllers: [AppController],
  providers: [
    AppService,
    ReaderListService,
    { provide: APP_GUARD, useClass: ThrottlerGuard }
  ],
})
export class AppModule {}
