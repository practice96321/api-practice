export class IUserList {
  name         : string;
  type         : number;
  description ?: string;
}
