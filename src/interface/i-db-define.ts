import { Schema } from "mongoose";

export interface IDbDefine{
  queryAll(): Promise<any[]>;

  query(queryData: any): Promise<any[]>;

  queryById(_id: Schema.Types.ObjectId): Promise<any>;

  create(createData: any): Promise<any>;

  update(updateData: any): Promise<any>;

  delete(_id: string): Promise<any>;

}