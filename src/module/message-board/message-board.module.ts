import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ManagementController } from 'src/management/management.controller';
import { MessageBoard, MessageBoardSchema } from 'src/schema/message-board.schema';
import { MessageBoardService } from './message-board.service';

@Module({
  providers: [MessageBoardService],
  controllers: [
  ],
  imports: [
  ]
})
export class MessageBoardModule {}
