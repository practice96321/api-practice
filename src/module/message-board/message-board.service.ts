import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Schema } from 'mongoose';
import { ResopnseDto } from 'src/dto/response.dto';
import { IDbDefine } from 'src/interface/i-db-define';
import { MessageBoard, MessageBoardDocument } from 'src/schema/message-board.schema';
import { GetMessageBoardDto } from './dto/get-message-board.dto';
import { MessageBoardDto } from './dto/message-board.dto';

@Injectable()
export class MessageBoardService implements IDbDefine {

  constructor (
    @InjectModel(MessageBoard.name)
    private readonly mongoose: Model<MessageBoardDocument>
  ) {}

  async queryAll(): Promise<MessageBoard[]> {
    return await this.mongoose.find().exec();
  };
  
  async query(queryData: GetMessageBoardDto): Promise<MessageBoard[]> {
    let response: ResopnseDto;
    let messageBoardDtos: MessageBoardDto[];
    // let _sort = { [queryData.sort_str] : queryData.sort };
    messageBoardDtos = await this.mongoose
      .find()
      .skip(queryData.skip || 0)
      .sort({[queryData.sort_str] : queryData.sort})
      .exec();
    return messageBoardDtos;
  };

  async queryById(_id: Schema.Types.ObjectId): Promise<any> {
    return await this.mongoose.findById(_id);
  };


  async create(createData: MessageBoardDto): Promise<MessageBoard> {
    // let _db = await new this.mongoose({
    //   ...createData
    //   // ordinal: string,
    //   // name: string,
    //   // message: string,
    //   // file_path: string,
    //   // file_name: string
    // }).save();
    return await new this.mongoose({
      ...createData
      // ordinal: string,
      // name: string,
      // message: string,
      // file_path: string,
      // file_name: string
    }).save();
  }

  async update(): Promise<MessageBoard> {
    return await this.mongoose.findOneAndUpdate().exec();
  }

  async delete(): Promise<any> {
    return await this.mongoose.findOneAndUpdate().exec();
  }
}
