import { IsNotEmpty, IsOptional, IsPositive } from 'class-validator';
import { MessageBoardDto } from "./message-board.dto";

export class GetMessageBoardDto extends MessageBoardDto {
  @IsOptional()
  sort?: number;

  @IsOptional()
  sort_str?: string;

  @IsPositive()
  @IsOptional()
  skip?: number;
  
  @IsPositive()
  @IsOptional()
  limit?: number;
}