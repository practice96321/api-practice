
export class MessageBoardDto {
  ordinal: string;
  name: string;
  message: string;
  file_path: string;
  file_name: string;
  // create_timestamp: Date;
}
