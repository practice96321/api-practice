import { Module } from '@nestjs/common';
import { UserListService } from './user-list.service';

@Module({
  providers: [UserListService]
})
export class UserListModule {}
