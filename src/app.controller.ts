import { Controller, Get, Request, Post, UseGuards, Param, Query, Body, Headers, Header } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AppService } from './app.service';
import { AuthService } from './auth/auth.service';
import { JwtAuthGuard } from './auth/jwt-auth.guard';
import { LocalAuthGuard } from './auth/local-auth-guard';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private authService: AuthService
    ) {}

  // @Get()
  // getHello(): string {
  //   return this.appService.getHello();
  // };
  
  @UseGuards(JwtAuthGuard)
  @Get('profile')
  // @Header('X-RateLimit-Limit', '1')
  getProfile(@Request() req) {
    return req.user;
  }
  
  // @UseGuards(AuthGuard('local'))
  @UseGuards(LocalAuthGuard)
  @Post('auth/login')
  async login(@Request() req: any) {
    // console.log('[login] req: ' + JSON.stringify(req));
    console.log('[login] req: ' + JSON.stringify(req.user));
    return this.authService.login(req.user);
    // return req.user;
  }
}
