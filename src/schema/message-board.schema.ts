import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

export type MessageBoardDocument = MessageBoard & Document;

@Schema()
export class MessageBoard {
  @Prop()
  ordinal: string;
  
  @Prop()
  name: string;

  @Prop()
  message: string;

  @Prop()
  file_path: string;

  @Prop()
  file_name: string;
};

export const MessageBoardSchema = SchemaFactory.createForClass(MessageBoard);
