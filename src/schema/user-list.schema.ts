import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

export type UserListDocument = User_List & Document;

@Schema()
export class User_List {
  @Prop({ required: true,  })
  name: string;

  @Prop()
  description?: string;

  @Prop()
  aaaa?: string;

  @Prop()
  bbbb?: string;

}

export const UserListSchema = SchemaFactory.createForClass(User_List);