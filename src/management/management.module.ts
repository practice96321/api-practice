import { Module } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { MongooseModule } from '@nestjs/mongoose';
import { ThrottlerGuard } from '@nestjs/throttler';
import { MessageBoardService } from 'src/module/message-board/message-board.service';
import { MessageBoard, MessageBoardSchema } from 'src/schema/message-board.schema';
import { User_List, UserListSchema } from 'src/schema/user-list.schema';
import { ManagementController } from './management.controller';
import { ManagementService } from './management.service';

@Module({
  providers: [
    ManagementService,
    MessageBoardService,
    { provide: APP_GUARD, useClass: ThrottlerGuard }
  ],
  controllers: [
    ManagementController
  ],
  imports: [
    MongooseModule.forFeature([
      { name : User_List.name, schema : UserListSchema},
      { name : MessageBoard.name, schema : MessageBoardSchema}
    ])
  ]
})
export class ManagementModule {}
