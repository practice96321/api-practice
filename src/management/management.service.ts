import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IInsertUserList } from 'src/interface/i-insert-user-list.interface';
import { IUserList } from 'src/interface/i-user-list.interface';
import { User_List, UserListDocument } from 'src/schema/user-list.schema';

@Injectable()
export class ManagementService {
  // schema: string = new Model().name;
  constructor(
    @InjectModel(User_List.name)
    private readonly model: Model<UserListDocument>
  ) {}

  async queryAll(): Promise<User_List[]> {
    return await this.model.find().exec();
  }
  
  async queryOne(query: IUserList): Promise<User_List[]> {
    return await this.model.find({
      ...query
    }).exec();
  }

  async create(create: IInsertUserList): Promise<User_List> {
    console.log('ManagementService', create);
    return await new this.model({
      ...create,
      createdAt: new Date(),
    }).save();
  }
}
