import { Body, Controller, Get, NotFoundException, Post, Query, UseGuards } from '@nestjs/common';
import { IInsertUserList } from 'src/interface/i-insert-user-list.interface';
import { MessageBoardService } from 'src/module/message-board/message-board.service';
import { ManagementService } from './management.service';
import { ResopnseDto } from '../dto/response.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('ManagementService.cgi')
export class ManagementController {
  // private readonly db: ManagementService;
  constructor (
    private readonly managementService: ManagementService,
    private readonly messageBoardService: MessageBoardService,
    // private readonly db: ManagementService
  ) {}

  @Get()
  async getAPI(@Query() query: any) {
    console.log(query);
    if (query.action == 'getDBUL') {
      return this.managementService.queryAll();
    } else if (query.action == 'getDBMSG') {
      return this.messageBoardService.queryAll();
    } else {
      throw new NotFoundException(null, 'Not found API.');
    };
  };

  @UseGuards(JwtAuthGuard)
  @Post()
  async insterAPI(@Body() inserJson: any) {
    // async insterAPI(@Body() inserJson: IInsertUserList) {
    console.log('ManagementController', inserJson);
    if (inserJson.action == 'insDBUL') {
      return await this.managementService.create(inserJson);
    } else if (inserJson.action == 'insDBMSG') {
      return await this.messageBoardService.create(inserJson);
    } else {
      throw new NotFoundException(null, 'Not found API.');
    };
  }
};
